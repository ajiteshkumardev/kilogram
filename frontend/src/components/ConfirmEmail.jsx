
const ConfirmEmail = () => {
    return (
        <div className="confirmEmail-prompt-parent">
            <div className="confirmEmail-prompt">
                <h1>Confirm your email.</h1>
                <p>You may close this tab now.</p>
            </div>
        </div>
    )
}
export default ConfirmEmail;